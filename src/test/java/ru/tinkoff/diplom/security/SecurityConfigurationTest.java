package ru.tinkoff.diplom.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.diplom.ApplicationIntegrationTest;
import ru.tinkoff.diplom.model.Teacher;
import ru.tinkoff.diplom.model.User;
import ru.tinkoff.diplom.service.UserService;

import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class SecurityConfigurationTest extends ApplicationIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private UserService userService;

    @Test
    @Transactional
    @Sql(statements = "insert into users(id, username, password, enabled, authority) values('c3bff918-945a-4fce-99a6-fdf3e6331c86', 'testUser','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_ADMIN')")
    public void roleAccessSuccessTest() throws Exception {
        mockMvc.perform(get("/courses/").with(httpBasic("testUser","password")))
                .andExpect(status().isOk());
        mockMvc.perform(get("/courses/").with(httpBasic("testUser","password")))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithAnonymousUser
    public void roleForbiddenAnonymousTest() throws Exception {
        mockMvc.perform(get("/classes/"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(get("/courses/"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(get("/user/"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(put("/teacher/"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(put("/user/"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(put("/courses/"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(delete("/courses/"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(delete("/teacher/"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(delete("/user/"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(post("/courses/"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(post("/classes/"))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(post("/user/"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "ERROR")
    public void roleForbiddenAccessUnsuccessfulTest() throws Exception {
        mockMvc.perform(get("/teacher/"))
                .andExpect(status().isForbidden());
        mockMvc.perform(get("/courses/"))
                .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    public void userUnauthorisedTest() throws Exception {
        mockMvc.perform(get("/classes/").with(httpBasic("asg","password")))
                .andExpect(status().isUnauthorized());
        mockMvc.perform(get("/courses/").with(httpBasic("123","password")))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "ADMIN")
    @Sql(statements = "insert into users(id, username, password, enabled, authority) values('c3bff918-945a-4fce-99a6-fdf3e6331c86', 'testAdmin','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_ADMIN')")
    @Sql(statements = "insert into users(id, username, password, enabled, authority) values('a3aff918-945a-4fce-99a6-fdf3e6331c86', 'deleteTestAdmin','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_ADMIN')")
    public void checkAccessToUserAdminRoleTest() throws Exception {
        UUID id = UUID.fromString("a3aff918-945a-4fce-99a6-fdf3e6331c86");
        User expectedUser = User.builder()
                .id(id)
                .username("newadmin")
                .password("password")
                .enabled(true)
                .authority("ROLE_ADMIN")
                .build();
        String expectedContent = objectMapper.writeValueAsString(expectedUser);
        mockMvc.perform(delete("/user/" + id).with(httpBasic("testAdmin","password"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        expectedUser.setId(UUID.fromString("c3bff918-945a-4fce-99a6-fdf3e6331c86"));
        mockMvc.perform(put("/user/").with(httpBasic("testAdmin","password"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedContent))
                .andExpect(status().isOk());
        expectedUser.setUsername("NewAdmin");
        expectedContent = objectMapper.writeValueAsString(expectedUser);
        mockMvc.perform(post("/user/").with(httpBasic("testAdmin","password"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedContent))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "USER")
    @Sql(statements = "insert into users(id, username, password, enabled, authority) values('c3bff918-945a-4fce-99a6-fdf3e6331c86', 'testUser','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_TEACHER')")
    @Sql(statements = "insert into users(id, username, password, enabled, authority) values('a3aff918-945a-4fce-99a6-fdf3e6331c86', 'deleteTestUser','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_TEACHER')")
    public void checkAccessToUserUserRoleTest() throws Exception {
        UUID id = UUID.fromString("a3aff918-945a-4fce-99a6-fdf3e6331c86");
        User expectedUser = User.builder()
                .id(id)
                .username("newadmin")
                .password("password")
                .enabled(true)
                .authority("ROLE_TEACHER")
                .build();
        String expectedContent = objectMapper.writeValueAsString(expectedUser);
        mockMvc.perform(delete("/user/" + id).with(httpBasic("testUser","password"))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
        mockMvc.perform(put("/user/").with(httpBasic("testUser","password"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedContent))
                .andExpect(status().isForbidden());
        mockMvc.perform(post("/user/").with(httpBasic("testUser","password"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedContent))
                .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "ADMIN",username = "admin")
    @Sql(statements = "insert into users(id, username, password, enabled, authority) values('c3bff918-945a-4fce-99a6-fdf3e6331c86', 'testAdmin','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_ADMIN')")
    @Sql(statements = "insert into teacher(id, lastname, firstname, second_name, age) values('c3bff918-945a-4fce-99a6-fdf3e6331c86', 'Kiselev', 'Dmitry', 'Sergeevich', '25')")
    public void checkAccessToTeacherAdminRoleTest() throws Exception {
        mockMvc.perform(get("/teacher/").with(httpBasic("testAdmin","password")))
                .andExpect(status().isOk());
        UUID id = UUID.fromString("c3bff918-945a-4fce-99a6-fdf3e6331c86");
        Teacher expectedTeacher = Teacher.builder()
                .id(id)
                .lastName("Kiselev")
                .firstName("Dmitry")
                .secondName("Sergeevich")
                .age(25).build();
        String expectedContent = objectMapper.writeValueAsString(expectedTeacher);
        mockMvc.perform(get("/teacher/" + id).with(httpBasic("testAdmin","password")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
        mockMvc.perform(delete("/teacher/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        mockMvc.perform(put("/teacher/").with(httpBasic("testAdmin","password"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedContent))
                .andExpect(status().isOk());
        expectedTeacher.setAge(18);
        expectedContent = objectMapper.writeValueAsString(expectedTeacher);
        mockMvc.perform(post("/teacher/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedContent))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithMockUser(roles = "TEACHER",username = "teacher")
    @Sql(statements = "insert into users(id, username, password, enabled, authority) values('c3bff918-945a-4fce-99a6-fdf3e6331c86', 'testAdmin','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_ADMIN')")
    @Sql(statements = "insert into teacher(id, lastname, firstname, second_name, age) values('c3bff918-945a-4fce-99a6-fdf3e6331c86', 'Kiselev', 'Dmitry', 'Sergeevich', '25')")
    public void checkAccessToTeacherRoleTest() throws Exception {
        mockMvc.perform(get("/teacher/").with(httpBasic("teacher","password")))
                .andExpect(status().isOk());
        UUID id = UUID.fromString("c3bff918-945a-4fce-99a6-fdf3e6331c86");
        Teacher expectedTeacher = Teacher.builder()
                .id(id)
                .lastName("Kiselev")
                .firstName("Dmitry")
                .secondName("Sergeevich")
                .age(25).build();
        String expectedContent = objectMapper.writeValueAsString(expectedTeacher);
        mockMvc.perform(get("/teacher/" + id).with(httpBasic("teacher","password")))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
        mockMvc.perform(delete("/teacher/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
        mockMvc.perform(put("/teacher/").with(httpBasic("teacher","password"))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(expectedContent))
                .andExpect(status().isForbidden());
    }
}