package ru.tinkoff.diplom.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.diplom.ApplicationIntegrationTest;
import ru.tinkoff.diplom.model.Lesson;
import ru.tinkoff.diplom.service.LessonService;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class LessonControllerTest extends ApplicationIntegrationTest{
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private LessonService service;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    void getByIdClassesSuccessTest() throws Exception {
        Lesson expectedCourse = Lesson.builder()
                .id(1)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date(null).build();
        String expectedContent = objectMapper.writeValueAsString(expectedCourse);
        mockMvc.perform(get("/classes/" + 1))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    void getByIdClassesUnsuccessfulTest() throws Exception {
        int id = 123;
        mockMvc.perform(get("/classes/" + id))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Lesson with id=" + id + " not exist"));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    void saveClassesSuccessTest() throws Exception {
        int id = 1;
        Lesson expectedClass = Lesson.builder()
                .id(id)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date("2014-03-12 13:37:27").build();
        String requestClasses = objectMapper.writeValueAsString(expectedClass);
        mockMvc.perform(put("/classes/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestClasses))
                .andExpect(status().isOk());
        Lesson actualClass = jdbcTemplate.queryForObject("select * from classes where id = ?",
                BeanPropertyRowMapper.newInstance(Lesson.class), id);
        Assertions.assertEquals(expectedClass, actualClass);
    }

    @Test
    @Transactional
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @WithMockUser(username = "admin", roles = "ADMIN")
    void saveClassesUnsuccessfulAddWithWrongDataTest() throws Exception {
        int id = 1;
        Lesson expectedClass = Lesson.builder()
                .id(id)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date("2014-03-12 13:37:27").build();
        String requestContent = objectMapper.writeValueAsString(expectedClass);
        mockMvc.perform(put("/classes/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Can't save lesson because lesson with id=" + expectedClass.getId() + " is already exist"));
        id = 2;
        expectedClass = Lesson.builder()
                .id(id)
                .name("     ")
                .description("").build();
        requestContent = objectMapper.writeValueAsString(expectedClass);
        mockMvc.perform(put("/classes/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteByIdClassesSuccessTest() throws Exception {
        int id = 1;
        mockMvc.perform(delete("/classes/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertThrows(IllegalArgumentException.class, () -> service.getClassesById(id));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteByIdClassesUnsuccessfulTest() throws Exception {
        int id = 1221;
        mockMvc.perform(delete("/classes/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Can't delete lesson because lesson with id=" + id + " not exist"));
    }

    @Test
    @Transactional
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @WithMockUser(username = "admin", roles = "ADMIN")
    void updateClassesSuccessTest() throws Exception {
        int id = 1;
        Lesson expectedClass = Lesson.builder()
                .id(id)
                .name("Name")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date("2014-03-12 13:37:27").build();
        String requestContent = objectMapper.writeValueAsString(expectedClass);
        mockMvc.perform(post("/classes/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isOk());
        Lesson actualClass = jdbcTemplate.queryForObject("select * from classes where id = ?",
                BeanPropertyRowMapper.newInstance(Lesson.class), id);
        Assertions.assertEquals(expectedClass, actualClass);
    }

    @Test
    @Transactional
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @WithMockUser(username = "admin", roles = "ADMIN")
    void updateClassesUnsuccessfulChangeWithNotExistClassesTest() throws Exception {
        int id = 2;
        Lesson expectedClass = Lesson.builder()
                .id(id)
                .name("Name")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date("2014-03-12 13:37:27").build();
        String requestContent = objectMapper.writeValueAsString(expectedClass);
        mockMvc.perform(post("/classes/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Can't update lesson because lesson with id=" + id + " not exist"));
    }

    @Test
    @Transactional
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @WithMockUser(username = "admin", roles = "ADMIN")
    void updateClassesUnsuccessfulChangeWithWrongDataTest() throws Exception {
        int id = 1;
        Lesson expectedClass = Lesson.builder()
                .id(id)
                .name(" ")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date("2014-03-12 13:37:27").build();
        String requestContent = objectMapper.writeValueAsString(expectedClass);
        mockMvc.perform(post("/classes/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isBadRequest());
    }
}