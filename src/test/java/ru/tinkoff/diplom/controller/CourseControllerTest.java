package ru.tinkoff.diplom.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.diplom.ApplicationIntegrationTest;
import ru.tinkoff.diplom.model.Lesson;
import ru.tinkoff.diplom.model.categories.Category;
import ru.tinkoff.diplom.model.categories.HumanitarianValue;
import ru.tinkoff.diplom.model.categories.TechnicalValue;
import ru.tinkoff.diplom.model.courses.Course;
import ru.tinkoff.diplom.model.type.OfflineValue;
import ru.tinkoff.diplom.model.type.OnlineValue;
import ru.tinkoff.diplom.model.type.Type;
import ru.tinkoff.diplom.service.course.CourseOfflineHumanitarianService;
import ru.tinkoff.diplom.service.course.CourseOfflineTechnicalService;
import ru.tinkoff.diplom.service.course.CourseOnlineHumanitarianService;
import ru.tinkoff.diplom.service.course.CourseOnlineTechnicalService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class CourseControllerTest<T extends Type, C extends Category, L extends Lesson> extends ApplicationIntegrationTest {

    private final CourseOnlineTechnicalService<T,C,L> courseOnlineTechnicalService;
    private final CourseOnlineHumanitarianService<T,C,L> courseOnlineHumanitarianService;
    private final CourseOfflineTechnicalService<T,C,L> courseOfflineTechnicalService;
    private final CourseOfflineHumanitarianService<T,C,L> courseOfflineHumanitarianService;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    public CourseControllerTest(CourseOnlineTechnicalService<T, C, L> courseOnlineTechnicalService, CourseOnlineHumanitarianService<T, C, L> courseOnlineHumanitarianService, CourseOfflineTechnicalService<T, C, L> courseOfflineTechnicalService, CourseOfflineHumanitarianService<T, C, L> courseOfflineHumanitarianService) {
        this.courseOnlineTechnicalService = courseOnlineTechnicalService;
        this.courseOnlineHumanitarianService = courseOnlineHumanitarianService;
        this.courseOfflineTechnicalService = courseOfflineTechnicalService;
        this.courseOfflineHumanitarianService = courseOfflineHumanitarianService;
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into online(id, url) values(1, 'test.ru')")
    @Sql(statements = "insert into technical(id, language, technology, toolkit) values(1, 'java', 'Spring', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void getByIdCourseSuccessTest() throws Exception {
        UUID id =UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86");
        OnlineValue type= new OnlineValue(1,"test.ru");
        TechnicalValue category = new TechnicalValue(1, "java", "Spring","InteliJ Idea");
        Lesson lesson = Lesson.builder()
                .id(1)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date(null).build();
        List<Lesson> list = new ArrayList<>();
        list.add(lesson);
        Course<OnlineValue, TechnicalValue, Lesson> expectedCourse =
                new Course<>(1,"TestCourse","test",type,id, category, list);
        String expectedContent = objectMapper.writeValueAsString(expectedCourse);
        mockMvc.perform(get("/courses/OnT/" + 1))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getByIdCourseUnsuccessfulTest() throws Exception {
        int id = 123;
        mockMvc.perform(get("/courses/OnT/" + id))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Course with id=" + id + " not exist"));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into online(id, url) values(1, 'test.ru')")
    @Sql(statements = "insert into technical(id, language, technology, toolkit) values(1, 'java', 'Spring', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void updateCourseUnsuccessfulChangeWithWrongDataTest() throws Exception {
        UUID id =UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86");
        OnlineValue type= new OnlineValue(1,"test.ru");
        TechnicalValue category = new TechnicalValue(1, "java", "Spring","InteliJ Idea");
        Lesson lesson = Lesson.builder()
                .id(1)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date(null).build();
        List<Lesson> list = new ArrayList<>();
        list.add(lesson);
        Course<OnlineValue, TechnicalValue, Lesson> expectedCourse =
                new Course<>(1," ","test",type,id, category, list);
        String requestContent = objectMapper.writeValueAsString(expectedCourse);
        mockMvc.perform(post("/courses/OnT/online/technical")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into online(id, url) values(1, 'test.ru')")
    @Sql(statements = "insert into technical(id, language, technology, toolkit) values(1, 'java', 'Spring', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void updateCourseUnsuccessfulChangeWithNotExistCourseTest() throws Exception {
        UUID id =UUID.fromString("a2bfa918-945a-4fce-99a6-fdf3e6331c86");
        OnlineValue type= new OnlineValue(1,"test.ru");
        TechnicalValue category = new TechnicalValue(1, "java", "Spring","InteliJ Idea");
        Lesson lesson = Lesson.builder()
                .id(1)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date(null).build();
        List<Lesson> list = new ArrayList<>();
        list.add(lesson);
        Course<OnlineValue, TechnicalValue, Lesson> expectedCourse =
                new Course<>(1," ","test",type,id, category, list);
        String requestContent = objectMapper.writeValueAsString(expectedCourse);
        mockMvc.perform(post("/courses/OnT/online/technical")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteByIdCourseUnsuccessfulTest() throws Exception {
        int id = 6;
        mockMvc.perform(delete("/courses/OnT/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into online(id, url) values(1, 'test.ru')")
    @Sql(statements = "insert into technical(id, language, technology, toolkit) values(1, 'java', 'Spring', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteByIdCourseSuccessTest() throws Exception {
        mockMvc.perform(delete("/courses/OnT/" + 1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertThrows(IllegalArgumentException.class, () -> courseOnlineTechnicalService.getCourseWithTypeAndCategoryAndLessons(1));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into offline(id,name_of_institution, address) values(1, 'RGRTU','Ryazan')")
    @Sql(statements = "insert into technical(id, language, technology, toolkit) values(1, 'java', 'Spring', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void getByIdCourseSuccessTestOfflineTechnical() throws Exception {
        UUID id =UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86");
        OfflineValue type= new OfflineValue(1,"RGRTU","Ryazan");
        TechnicalValue category = new TechnicalValue(1, "java", "Spring","InteliJ Idea");
        Lesson lesson = Lesson.builder()
                .id(1)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date(null).build();
        List<Lesson> list = new ArrayList<>();
        list.add(lesson);
        Course<OfflineValue, TechnicalValue, Lesson> expectedCourse =
                new Course<>(1,"TestCourse","test",type,id, category, list);
        String expectedContent = objectMapper.writeValueAsString(expectedCourse);
        mockMvc.perform(get("/courses/OffT/" + 1))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getByIdCourseUnsuccessfulTestOfflineTechnical() throws Exception {
        int id = 123;
        mockMvc.perform(get("/courses/OffT/" + id))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Course with id=" + id + " not exist"));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into offline(id,name_of_institution, address) values(1, 'RGRTU','Ryazan')")
    @Sql(statements = "insert into technical(id, language, technology, toolkit) values(1, 'java', 'Spring', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void updateCourseUnsuccessfulChangeWithWrongDataTestOfflineTechnical() throws Exception {
        UUID id =UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86");
        OfflineValue type= new OfflineValue(1,"RGRTU","Ryazan");
        TechnicalValue category = new TechnicalValue(1, "java", "Spring","InteliJ Idea");
        Lesson lesson = Lesson.builder()
                .id(1)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date(null).build();
        List<Lesson> list = new ArrayList<>();
        list.add(lesson);
        Course<OfflineValue, TechnicalValue, Lesson> expectedCourse =
                new Course<>(1," ","test",type,id, category, list);
        String requestContent = objectMapper.writeValueAsString(expectedCourse);
        mockMvc.perform(post("/courses/OffT/offline/technical")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into offline(id,name_of_institution, address) values(1, 'RGRTU','Ryazan')")
    @Sql(statements = "insert into technical(id, language, technology, toolkit) values(1, 'java', 'Spring', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void updateCourseUnsuccessfulChangeWithNotExistCourseTestOfflineTechnical() throws Exception {
        UUID id =UUID.fromString("a2bfa918-945a-4fce-99a6-fdf3e6331c86");
        OfflineValue type= new OfflineValue(1,"RGRTU","Ryazan");
        TechnicalValue category = new TechnicalValue(1, "java", "Spring","InteliJ Idea");
        Lesson lesson = Lesson.builder()
                .id(1)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date(null).build();
        List<Lesson> list = new ArrayList<>();
        list.add(lesson);
        Course<OfflineValue, TechnicalValue, Lesson> expectedCourse =
                new Course<>(1," ","test",type,id, category, list);
        String requestContent = objectMapper.writeValueAsString(expectedCourse);
        mockMvc.perform(post("/courses/OffT/offline/technical")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteByIdCourseUnsuccessfulTestOfflineTechnical() throws Exception {
        int id = 6;
        mockMvc.perform(delete("/courses/OffT/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into offline(id,name_of_institution, address) values(1, 'RGRTU','Ryazan')")
    @Sql(statements = "insert into technical(id, language, technology, toolkit) values(1, 'java', 'Spring', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void deleteByIdCourseSuccessTestOfflineTechnical() throws Exception {
        mockMvc.perform(delete("/courses/OffT/" + 1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertThrows(IllegalArgumentException.class, () -> courseOfflineTechnicalService.getCourseWithTypeAndCategoryAndLessons(1));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into online(id, url) values(1, 'test.ru')")
    @Sql(statements = "insert into humanitarian(id, language, toolkit) values(1, 'java', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void getByIdCourseSuccessTestOnlineHumanitarian() throws Exception {
        UUID id =UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86");
        OnlineValue type= new OnlineValue(1,"test.ru");
        HumanitarianValue category = new HumanitarianValue(1, "java", "InteliJ Idea");
        Lesson lesson = Lesson.builder()
                .id(1)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date(null).build();
        List<Lesson> list = new ArrayList<>();
        list.add(lesson);
        Course<OnlineValue, HumanitarianValue, Lesson> expectedCourse =
                new Course<>(1,"TestCourse","test",type,id, category, list);
        String expectedContent = objectMapper.writeValueAsString(expectedCourse);
        mockMvc.perform(get("/courses/OnH/" + 1))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getByIdCourseUnsuccessfulTestOnlineHumanitarian() throws Exception {
        int id = 123;
        mockMvc.perform(get("/courses/OnH/" + id))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Course with id=" + id + " not exist"));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into online(id, url) values(1, 'test.ru')")
    @Sql(statements = "insert into humanitarian(id, language, toolkit) values(1, 'java', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void updateCourseUnsuccessfulChangeWithWrongDataTestOnlineHumanitarian() throws Exception {
        UUID id =UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86");
        OnlineValue type= new OnlineValue(1,"test.ru");
        HumanitarianValue category = new HumanitarianValue(1, "java", "InteliJ Idea");
        Lesson lesson = Lesson.builder()
                .id(1)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date(null).build();
        List<Lesson> list = new ArrayList<>();
        list.add(lesson);
        Course<OnlineValue, HumanitarianValue, Lesson> expectedCourse =
                new Course<>(1," ","test",type,id, category, list);
        String requestContent = objectMapper.writeValueAsString(expectedCourse);
        mockMvc.perform(post("/courses/OnH/online/humanitarian")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into online(id, url) values(1, 'test.ru')")
    @Sql(statements = "insert into humanitarian(id, language, toolkit) values(1, 'java', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void updateCourseUnsuccessfulChangeWithNotExistCourseTestOnlineHumanitarian() throws Exception {
        UUID id =UUID.fromString("a2bfa918-945a-4fce-99a6-fdf3e6331c86");
        OnlineValue type= new OnlineValue(1,"test.ru");
        HumanitarianValue category = new HumanitarianValue(1, "java", "InteliJ Idea");
        Lesson lesson = Lesson.builder()
                .id(1)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date(null).build();
        List<Lesson> list = new ArrayList<>();
        list.add(lesson);
        Course<OnlineValue, HumanitarianValue, Lesson> expectedCourse =
                new Course<>(1," ","test",type,id, category, list);
        String requestContent = objectMapper.writeValueAsString(expectedCourse);
        mockMvc.perform(post("/courses/OnH/online/humanitarian")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteByIdCourseUnsuccessfulTestOnlineHumanitarian() throws Exception {
        int id = 6;
        mockMvc.perform(delete("/courses/OnH/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into online(id, url) values(1, 'test.ru')")
    @Sql(statements = "insert into humanitarian(id, language, toolkit) values(1, 'java', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void deleteByIdCourseSuccessTestOnlineHumanitarian() throws Exception {
        mockMvc.perform(delete("/courses/OnH/" + 1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertThrows(IllegalArgumentException.class, () -> courseOnlineHumanitarianService.getCourseWithTypeAndCategoryAndLessons(1));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into offline(id,name_of_institution, address) values(1, 'RGRTU','Ryazan')")
    @Sql(statements = "insert into humanitarian(id, language, toolkit) values(1, 'java', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void getByIdCourseSuccessTestOfflineHumanitarian() throws Exception {
        UUID id =UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86");
        OfflineValue type= new OfflineValue(1,"RGRTU","Ryazan");
        HumanitarianValue category = new HumanitarianValue(1, "java", "InteliJ Idea");
        Lesson lesson = Lesson.builder()
                .id(1)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date(null).build();
        List<Lesson> list = new ArrayList<>();
        list.add(lesson);
        Course<OfflineValue, HumanitarianValue, Lesson> expectedCourse =
                new Course<>(1,"TestCourse","test",type,id, category, list);
        String expectedContent = objectMapper.writeValueAsString(expectedCourse);
        mockMvc.perform(get("/courses/OffH/" + 1))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getByIdCourseUnsuccessfulTestOfflineHumanitarian() throws Exception {
        int id = 123;
        mockMvc.perform(get("/courses/OffH/" + id))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Course with id=" + id + " not exist"));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into offline(id,name_of_institution, address) values(1, 'RGRTU','Ryazan')")
    @Sql(statements = "insert into humanitarian(id, language, toolkit) values(1, 'java', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void updateCourseUnsuccessfulChangeWithWrongDataTestOfflineHumanitarian() throws Exception {
        UUID id =UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86");
        OfflineValue type= new OfflineValue(1,"RGRTU","Ryazan");
        HumanitarianValue category = new HumanitarianValue(1, "java", "InteliJ Idea");
        Lesson lesson = Lesson.builder()
                .id(1)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date(null).build();
        List<Lesson> list = new ArrayList<>();
        list.add(lesson);
        Course<OfflineValue, HumanitarianValue, Lesson> expectedCourse =
                new Course<>(1," ","test",type,id, category, list);
        String requestContent = objectMapper.writeValueAsString(expectedCourse);
        mockMvc.perform(post("/courses/OffH/offline/humanitarian")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into offline(id,name_of_institution, address) values(1, 'RGRTU','Ryazan')")
    @Sql(statements = "insert into humanitarian(id, language, toolkit) values(1, 'java', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void updateCourseUnsuccessfulChangeWithNotExistCourseTestOfflineHumanitarian() throws Exception {
        UUID id =UUID.fromString("a2bfa918-945a-4fce-99a6-fdf3e6331c86");
        OfflineValue type= new OfflineValue(1,"RGRTU","Ryazan");
        HumanitarianValue category = new HumanitarianValue(1, "java", "InteliJ Idea");
        Lesson lesson = Lesson.builder()
                .id(1)
                .name("TestName")
                .description("test")
                .teacherId(UUID.fromString("a2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .date(null).build();
        List<Lesson> list = new ArrayList<>();
        list.add(lesson);
        Course<OfflineValue, HumanitarianValue, Lesson> expectedCourse =
                new Course<>(1," ","test",type,id, category, list);
        String requestContent = objectMapper.writeValueAsString(expectedCourse);
        mockMvc.perform(post("/courses/OffH/offline/humanitarian")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteByIdCourseUnsuccessfulTestOfflineHumanitarian() throws Exception {
        int id = 6;
        mockMvc.perform(delete("/courses/OffH/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'TestName', 'test', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'TestCourse', 'test', '1', 'a2bff918-945a-4fce-99a6-fdf3e6331c86', '1','1')")
    @Sql(statements = "insert into category(id, category_id) values(1, 1)")
    @Sql(statements = "insert into type_of_course(id, type_id) values(1, 1)")
    @Sql(statements = "insert into offline(id,name_of_institution, address) values(1, 'RGRTU','Ryazan')")
    @Sql(statements = "insert into humanitarian(id, language, toolkit) values(1, 'java', 'InteliJ Idea')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, 1)")
    void deleteByIdCourseSuccessTestOfflineHumanitarian() throws Exception {
        mockMvc.perform(delete("/courses/OffH/" + 1)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertThrows(IllegalArgumentException.class, () -> courseOfflineHumanitarianService.getCourseWithTypeAndCategoryAndLessons(1));
    }
}