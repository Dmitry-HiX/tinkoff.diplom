package ru.tinkoff.diplom.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.diplom.ApplicationIntegrationTest;
import ru.tinkoff.diplom.model.Teacher;
import ru.tinkoff.diplom.service.TeacherService;

import java.security.Principal;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class TeacherControllerTest  extends ApplicationIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TeacherService service;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "INSERT INTO users (id, username, password, enabled, authority) values('d2bff918-945a-4fce-99a6-fdf3e6331c86', 'test','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_TEACHER');")
    @Sql(statements = "insert into teacher(id, lastName, firstName, second_name, age) values('d2bff918-945a-4fce-99a6-fdf3e6331c86', 'test1', 'test2', 'test3', '20')")
    void getByIdTeacherSuccessTest() throws Exception {
        Teacher expectedTeacher = Teacher.builder()
                .id(UUID.fromString("d2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .lastName("test1")
                .firstName("test2")
                .secondName("test3")
                .age(20).build();
        String expectedContent = objectMapper.writeValueAsString(expectedTeacher);
        mockMvc.perform(get("/teacher/" + "d2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getByIdTeacherUnsuccessfulTest() throws Exception {
        UUID id = UUID.fromString("e2bff918-945a-4fce-99a6-fdf3e6331c86");
        mockMvc.perform(get("/teacher/" + id))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Teacher with id=" + id + " not exist"));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "INSERT INTO users (id, username, password, enabled, authority) values('d2bff918-945a-4fce-99a6-fdf3e6331c86', 'test','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_TEACHER');")
    @Sql(statements = "insert into teacher(id, lastName, firstName, second_name, age) values('d2bff918-945a-4fce-99a6-fdf3e6331c86', 'test1', 'test2', 'test3', '20')")
    void updateTeacherSuccessfulTest() throws Exception {
        Teacher expectedTeacher = Teacher.builder()
                .id(UUID.fromString("d2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .lastName("test1")
                .firstName("test2 ")
                .secondName("test3")
                .age(20).build();
        String requestContent = objectMapper.writeValueAsString(expectedTeacher);
        mockMvc.perform(post("/teacher/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "INSERT INTO users (id, username, password, enabled, authority) values('d2bff918-945a-4fce-99a6-fdf3e6331c86', 'test','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_TEACHER');")
    void saveTeacherSuccessfulTest() throws Exception {
        Teacher expectedTeacher = Teacher.builder()
                .id(UUID.fromString("d2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .lastName("test1")
                .firstName("test2 ")
                .secondName("test3")
                .age(20).build();
        String requestContent = objectMapper.writeValueAsString(expectedTeacher);
        mockMvc.perform(put("/teacher/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    @WithMockUser(username = "teacher", roles = "TEACHER")
    @Sql(statements = "INSERT INTO users (id, username, password, enabled, authority) values('d2bff918-945a-4fce-99a6-fdf3e6331c86', 'test','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_TEACHER');")
    void saveTeacherUnsuccessfulTest() throws Exception {
        Teacher expectedTeacher = Teacher.builder()
                .id(UUID.fromString("d2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .lastName("test1")
                .firstName("test2 ")
                .secondName("test3")
                .age(20).build();
        String requestContent = objectMapper.writeValueAsString(expectedTeacher);
        mockMvc.perform(put("/teacher/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "INSERT INTO users (id, username, password, enabled, authority) values('d2bff918-945a-4fce-99a6-fdf3e6331c86', 'test','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_TEACHER');")
    @Sql(statements = "insert into teacher(id, lastName, firstName, second_name, age) values('d2bff918-945a-4fce-99a6-fdf3e6331c86', 'test1', 'test2', 'test3', '20')")
    void updateTeacherUnsuccessfulChangeWithWrongDataTest() throws Exception {
        Authentication authentication = SecurityContextHolder.getContext()
                .getAuthentication();
        Principal principal= (Principal) authentication.getPrincipal();
        Teacher expectedTeacher = Teacher.builder()
                .id(UUID.fromString("d2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .lastName(" ")
                .firstName("test2 ")
                .secondName("test3")
                .age(20).build();
        String requestContent = objectMapper.writeValueAsString(expectedTeacher);
        mockMvc.perform(post("/teacher/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "INSERT INTO users (id, username, password, enabled, authority) values('d2bff918-945a-4fce-99a6-fdf3e6331c86', 'test','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_TEACHER');")
    @Sql(statements = "insert into teacher(id, lastName, firstName, second_name, age) values('d2bff918-945a-4fce-99a6-fdf3e6331c86', 'test1', 'test2', 'test3', '20')")
    void updateTeacherUnsuccessfulChangeWithNotExistTeacherTest() throws Exception {
        UUID id = UUID.fromString("e2bfa918-945a-4fce-99a6-fdf3e6331c86");
        Teacher expectedTeacher = Teacher.builder()
                .id(id)
                .lastName("test1")
                .firstName("test2 ")
                .secondName("test3")
                .age(20).build();
        String requestContent = objectMapper.writeValueAsString(expectedTeacher);
        mockMvc.perform(post("/teacher/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteByIdTeacherUnsuccessfulTest() throws Exception {
        UUID id = UUID.fromString("e2bff918-945a-4fce-99a6-fdf3e6331c86");
        mockMvc.perform(delete("/teacher/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Can't delete teacher because teacher with id=" + id + " not exist"));
    }

    @Test
    @Transactional
    @Sql(statements = "INSERT INTO users (id, username, password, enabled, authority) values('d2bff918-945a-4fce-99a6-fdf3e6331c86', 'test','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_TEACHER');")
    @Sql(statements = "insert into teacher(id, lastName, firstName, second_name, age) values('d2bff918-945a-4fce-99a6-fdf3e6331c86', 'test1', 'test2', 'test3', '20')")
    @WithMockUser(username = "admin", roles = "ADMIN")
    void deleteByIdClassesSuccessTest() throws Exception {
        UUID id = UUID.fromString("d2bff918-945a-4fce-99a6-fdf3e6331c86");
        mockMvc.perform(delete("/teacher/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        assertThrows(IllegalArgumentException.class, () -> service.getTeacherById(id));
    }
}