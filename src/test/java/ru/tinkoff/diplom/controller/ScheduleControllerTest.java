package ru.tinkoff.diplom.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.diplom.ApplicationIntegrationTest;
import ru.tinkoff.diplom.model.Schedule;
import ru.tinkoff.diplom.service.ScheduleService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
class ScheduleControllerTest extends ApplicationIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ScheduleService service;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    @Sql(statements = "INSERT INTO users (id, username, password, enabled, authority) values('c2bff918-945a-4fce-99a6-fdf3e6331c86', 'test','$2a$10$eFCxwAyFmZPfXnosWXEaZOVwug6k1FsaeIQvBKTmCYdRuCxLtNmu6', true, 'ROLE_TEACHER');")
    @Sql(statements = "insert into teacher(id, lastname, firstname, second_name, age) values('c2bff918-945a-4fce-99a6-fdf3e6331c86', 'Kiselev', 'Dmitry', 'Sergeevich', '25')")
    @Sql(statements = "insert into classes(id, name, description, teacher_id, date) values(1, 'classesTestName', 'test', 'c2bff918-945a-4fce-99a6-fdf3e6331c86', null)")
    @Sql(statements = "insert into course(id, name, description, type_id, curator_id, category_id, list_of_classes) values(1, 'courseTestName', 'test', '1', 'c2bff918-945a-4fce-99a6-fdf3e6331c86', '1', '1')")
    @Sql(statements = "insert into list_of_classes(id, list_id_classes) values(1, '1')")
    void getByIdScheduleSuccessTest() throws Exception {
        List<Schedule> listExpected = new ArrayList<>();
        Schedule expectedSchedule = Schedule.builder()
                .id(UUID.fromString("c2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .fioTeacher("Kiselev D.S.")
                .courseName("courseTestName")
                .classesName("classesTestName")
                .date(null).build();
        listExpected.add(expectedSchedule);
        String expectedContent = objectMapper.writeValueAsString(listExpected);
        mockMvc.perform(get("/schedule/" + "c2bff918-945a-4fce-99a6-fdf3e6331c86"))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @Transactional
    @WithMockUser(username = "admin", roles = "ADMIN")
    void getByIdScheduleUnsuccessfulTest() throws Exception {
        UUID id = UUID.fromString("c2bff918-945a-4fce-99a6-fdf3e6331c86");
        mockMvc.perform(get("/schedule/" + id))
                .andExpect(status().isNotFound())
                .andExpect(content().string("Schedule with id=" + id + " not exist"));
    }
}