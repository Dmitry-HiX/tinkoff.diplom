package ru.tinkoff.diplom.model;

import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Teacher {
    @NotNull
    private UUID id;
    @NotBlank
    String lastName;
    @NotBlank
    String firstName;
    @NotBlank
    String secondName;
    @Min(18)
    int age;
}
