package ru.tinkoff.diplom.model.categories;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class TechnicalValue extends Category implements Serializable {
    @NotBlank
    private String language;
    @NotBlank
    private String technology;
    @NotBlank
    private String toolkit;

    @Builder
    public TechnicalValue(int id, String language, String technology,String toolkit) {
        super(id);
        this.language = language;
        this.technology = technology;
        this.toolkit = toolkit;
    }
}
