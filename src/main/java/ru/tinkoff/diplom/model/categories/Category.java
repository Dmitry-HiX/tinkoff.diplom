package ru.tinkoff.diplom.model.categories;

import lombok.*;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category {
    @NotNull
    private int id;
}
