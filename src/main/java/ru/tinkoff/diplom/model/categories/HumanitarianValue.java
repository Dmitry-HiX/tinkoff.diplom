package ru.tinkoff.diplom.model.categories;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class HumanitarianValue extends Category implements Serializable {
    @NotBlank
    private String language;
    @NotBlank
    private String toolkit;

    @Builder
    public HumanitarianValue(int id, String language, String toolkit) {
        super(id);
        this.language = language;
        this.toolkit = toolkit;
    }
}
