package ru.tinkoff.diplom.model.type;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Type {
    @NotNull
    private int id;
}
