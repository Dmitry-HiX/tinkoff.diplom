package ru.tinkoff.diplom.model.type;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class OnlineValue extends Type implements Serializable {
    @NotBlank
    private String url;

    @Builder
    public OnlineValue(int id, String url) {
        super(id);
        this.url = url;
    }
}
