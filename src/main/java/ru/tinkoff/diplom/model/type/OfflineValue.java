package ru.tinkoff.diplom.model.type;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class OfflineValue extends Type implements Serializable {
    @NotBlank
    private String nameOfInstitution;
    @NotBlank
    private String address;

    @Builder
    public OfflineValue(int id, String nameOfInstitution, String address) {
        super(id);
        this.nameOfInstitution = nameOfInstitution;
        this.address = address;
    }
}
