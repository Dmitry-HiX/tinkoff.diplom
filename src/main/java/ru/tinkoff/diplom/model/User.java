package ru.tinkoff.diplom.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class User {
    @NotNull
    private UUID id;
    @NotBlank
    private String username;
    @NotBlank
    private String password;
    @Builder.Default
    @NotBlank
    private String authority = "ROLE_TEACHER";
    @NotNull
    private boolean enabled;
}
