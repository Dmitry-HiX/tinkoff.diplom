package ru.tinkoff.diplom.model.courses;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tinkoff.diplom.model.Lesson;
import ru.tinkoff.diplom.model.categories.Category;
import ru.tinkoff.diplom.model.type.Type;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@Builder
public class Course<T extends Type,C extends Category, L extends Lesson> {
    @NotNull
    private int id;
    @NotBlank
    private String name;
    @NotBlank
    private String description;
    @NotNull
    private T type;
    @NotNull
    private UUID curatorId;
    @NotNull
    private C category;
    @NotNull
    private List<L> lessons;

    public Course(int id, String name, String description, T type, UUID curatorId, C category, List<L> lessons) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        this.curatorId = curatorId;
        this.category = category;
        this.lessons = lessons;
    }
}
