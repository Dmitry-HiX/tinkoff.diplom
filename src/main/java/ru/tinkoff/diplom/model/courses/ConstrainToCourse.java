package ru.tinkoff.diplom.model.courses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConstrainToCourse {
    @NotNull
    private int courseId;
    @NotNull
    private int constrainId;
}
