package ru.tinkoff.diplom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.diplom.model.Schedule;
import ru.tinkoff.diplom.service.ScheduleService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/schedule")
public class ScheduleController {
    @Autowired
    ScheduleService scheduleService;

    @GetMapping("/{id}")
    public List<Schedule> getById(@PathVariable("id") UUID id) {
        return scheduleService.findScheduleById(id);
    }
}
