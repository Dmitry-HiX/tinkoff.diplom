package ru.tinkoff.diplom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.diplom.model.Lesson;
import ru.tinkoff.diplom.model.categories.Category;
import ru.tinkoff.diplom.model.categories.HumanitarianValue;
import ru.tinkoff.diplom.model.categories.TechnicalValue;
import ru.tinkoff.diplom.model.courses.ConstrainToCourse;
import ru.tinkoff.diplom.model.courses.Course;
import ru.tinkoff.diplom.model.courses.DefaultCourse;
import ru.tinkoff.diplom.model.type.OfflineValue;
import ru.tinkoff.diplom.model.type.OnlineValue;
import ru.tinkoff.diplom.model.type.Type;
import ru.tinkoff.diplom.service.course.CourseOfflineHumanitarianService;
import ru.tinkoff.diplom.service.course.CourseOfflineTechnicalService;
import ru.tinkoff.diplom.service.course.CourseOnlineHumanitarianService;
import ru.tinkoff.diplom.service.course.CourseOnlineTechnicalService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/courses")
public class CourseController<T extends Type, C extends Category, L extends Lesson> {
    private final CourseOfflineHumanitarianService<T, C, L> courseOfflineHumanitarianService;
    private final CourseOnlineHumanitarianService<T, C, L> courseOnlineHumanitarianService;
    private final CourseOfflineTechnicalService<T, C, L> courseOfflineTechnicalService;
    private final CourseOnlineTechnicalService<T, C, L> courseOnlineTechnicalService;

    @Autowired
    public CourseController(CourseOfflineHumanitarianService<T, C, L> courseOfflineHumanitarianService, CourseOnlineHumanitarianService<T, C, L> courseOnlineHumanitarianService, CourseOfflineTechnicalService<T, C, L> courseOfflineTechnicalService, CourseOnlineTechnicalService<T, C, L> courseOnlineTechnicalService) {
        this.courseOfflineHumanitarianService = courseOfflineHumanitarianService;
        this.courseOnlineHumanitarianService = courseOnlineHumanitarianService;
        this.courseOfflineTechnicalService = courseOfflineTechnicalService;
        this.courseOnlineTechnicalService = courseOnlineTechnicalService;
    }

    @GetMapping
    public List<DefaultCourse> findAll() {
        return courseOfflineHumanitarianService.findAll();
    }

    @GetMapping("/OnT/{idCourse}")
    public Course<OnlineValue, TechnicalValue, ?> getByIdFull(@PathVariable int idCourse) {
        return courseOnlineTechnicalService.getCourseWithTypeAndCategoryAndLessons(idCourse);
    }

    @PutMapping("/OnT/online/technical")
    public void save(@RequestBody @Valid Course<OnlineValue, TechnicalValue, Lesson> course, Principal principal) {
        courseOnlineTechnicalService.saveCourse(course, principal);
    }

    @PostMapping("/OnT/online/technical")
    public void update(@RequestBody @Valid Course<OnlineValue, TechnicalValue, Lesson> course, Principal principal) {
        courseOnlineTechnicalService.updateCourse(course, principal);
    }

    @DeleteMapping("/OnT/{id}")
    public void delete(@PathVariable int id, Principal principal) {
        courseOnlineTechnicalService.deleteCourse(id, principal);
    }

    @PutMapping ("/classesToCourses")
    public void saveClassesToCourse(@RequestBody @Valid ConstrainToCourse constrainToCourse, Principal principal) {
        courseOnlineTechnicalService.saveClassesToCourse(constrainToCourse, principal);
    }

    @PutMapping ("/OnT/copy/")
    public void copyCourseOnlineTechnical(@RequestBody @Valid Course<OnlineValue, TechnicalValue, Lesson> course, Principal principal) {
        courseOnlineTechnicalService.copyCourse(course, principal);
    }

    @GetMapping("/OnT/full")
    public List<DefaultCourse> getAllCourseOnlineTechnical(){
        return courseOnlineTechnicalService.getAllCourseOnlineTechnical();
    }

    /////////////////////

    @GetMapping("/OffT/{idCourse}")
    public Course<OfflineValue, TechnicalValue, ?> getByIdFullOfflineTechnical(@PathVariable int idCourse) {
        return courseOfflineTechnicalService.getCourseWithTypeAndCategoryAndLessons(idCourse);
    }

    @PutMapping("/OffT/offline/technical")
    public void saveOfflineTechnical(@RequestBody @Valid Course<OfflineValue, TechnicalValue, Lesson> course, Principal principal) {
        courseOfflineTechnicalService.saveCourse(course, principal);
    }

    @PostMapping("/OffT/offline/technical")
    public void updateOfflineTechnical(@RequestBody @Valid Course<OfflineValue, TechnicalValue, Lesson> course, Principal principal) {
        courseOfflineTechnicalService.updateCourse(course, principal);
    }

    @DeleteMapping("/OffT/{id}")
    public void deleteOfflineTechnical(@PathVariable int id, Principal principal) {
        courseOfflineTechnicalService.deleteCourse(id, principal);
    }

    @PutMapping ("/OffT/copy/")
    public void copyCourseOfflineTechnical(@RequestBody @Valid Course<OfflineValue, TechnicalValue, Lesson> course, Principal principal) {
        courseOfflineTechnicalService.copyCourse(course, principal);
    }

    @GetMapping("/OffT/full")
    public List<DefaultCourse> getAllCourseOfflineTechnical(){
        return courseOfflineTechnicalService.getAllCourseOfflineTechnical();
    }

    /////////////////////

    @GetMapping("/OnH/{idCourse}")
    public Course<OnlineValue, HumanitarianValue, ?> getByIdFullOnlineHumanitarian(@PathVariable int idCourse) {
        return courseOnlineHumanitarianService.getCourseWithTypeAndCategoryAndLessons(idCourse);
    }

    @PutMapping("/OnH/online/humanitarian")
    public void saveOnlineHumanitarian(@RequestBody @Valid Course<OnlineValue, HumanitarianValue, Lesson> course,Principal principal) {
        courseOnlineHumanitarianService.saveCourse(course, principal);
    }

    @PostMapping("/OnH/online/humanitarian")
    public void updateOnlineHumanitarian(@RequestBody @Valid Course<OnlineValue, HumanitarianValue, Lesson> course, Principal principal) {
        courseOnlineHumanitarianService.updateCourse(course, principal);
    }

    @DeleteMapping("/OnH/{id}")
    public void deleteOnlineHumanitarian(@PathVariable int id, Principal principal) {
        courseOnlineHumanitarianService.deleteCourse(id, principal);
    }

    @PutMapping ("/OnH/copy/")
    public void copyCourseOnlineHumanitarian(@RequestBody @Valid Course<OnlineValue, HumanitarianValue, Lesson> course, Principal principal) {
        courseOnlineHumanitarianService.copyCourse(course, principal);
    }

    @GetMapping("/OnH/full")
    public List<DefaultCourse> getAllCourseOnlineHumanitarian(){
        return courseOnlineHumanitarianService.getAllCourseOnlineHumanitarian();
    }

    /////////////////////

    @GetMapping("/OffH/{idCourse}")
    public Course<OfflineValue, HumanitarianValue, ?> getByIdFullOfflineHumanitarian(@PathVariable int idCourse) {
        return courseOfflineHumanitarianService.getCourseWithTypeAndCategoryAndLessons(idCourse);
    }

    @PutMapping("/OffH/offline/humanitarian")
    public void saveOfflineHumanitarian(@RequestBody @Valid Course<OfflineValue, HumanitarianValue, Lesson> course, Principal principal) {
        courseOfflineHumanitarianService.saveCourse(course, principal);
    }

    @PostMapping("/OffH/offline/humanitarian")
    public void updateOfflineHumanitarian(@RequestBody @Valid Course<OfflineValue, HumanitarianValue, Lesson> course, Principal principal) {
        courseOfflineHumanitarianService.updateCourse(course, principal);
    }

    @DeleteMapping("/OffH/{id}")
    public void deleteOfflineHumanitarian(@PathVariable int id, Principal principal) {
        courseOfflineHumanitarianService.deleteCourse(id, principal);
    }

    @PutMapping ("/OffH/copy/")
    public void copyCourseOfflineHumanitarian(@RequestBody @Valid Course<OfflineValue, HumanitarianValue, Lesson> course, Principal principal) {
        courseOfflineHumanitarianService.copyCourse(course, principal);
    }

    @GetMapping("/OffH/full")
    public List<DefaultCourse> getAllCourseOfflineHumanitarian(){
        return courseOfflineHumanitarianService.getAllCourseOfflineHumanitarian();
    }
}
