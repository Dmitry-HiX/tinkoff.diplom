package ru.tinkoff.diplom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.diplom.model.Teacher;
import ru.tinkoff.diplom.service.TeacherService;
import ru.tinkoff.diplom.service.UserService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    public Teacher getById(@PathVariable("id") UUID id) {
        return teacherService.getTeacherById(id);
    }

    @GetMapping
    public List<Teacher> findAll() {
        return teacherService.findAllTeacher();
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteById(@PathVariable("id") UUID id) {
        teacherService.deleteTeacher(id);
    }

    @PostMapping
    public void update(@RequestBody @Valid Teacher teacher, Principal principal) {
        teacherService.updateTeacher(teacher, principal);
    }

    @PutMapping
    @PreAuthorize("hasRole('ADMIN')")
    public void save(@RequestBody @Valid Teacher teacher) {
        teacherService.saveTeacher(teacher);
    }
}
