package ru.tinkoff.diplom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.diplom.model.User;
import ru.tinkoff.diplom.service.UserService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public User getById(@PathVariable("id") UUID id) {
        return userService.getUserById(id);
    }

    @GetMapping("/{username}")
    @PreAuthorize("hasRole('ADMIN')")
    public User getByUsername(@PathVariable("username") String username) {
        return userService.getUserByUsername(username);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteById(@PathVariable("id") UUID id) {
        userService.deleteUser(id);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public void update(@RequestBody @Valid User user) {
        userService.updateUser(user);
    }

    @PutMapping
    @PreAuthorize("hasRole('ADMIN')")
    public void save(@RequestBody @Valid User user) {
        userService.saveUser(user);
    }
}
