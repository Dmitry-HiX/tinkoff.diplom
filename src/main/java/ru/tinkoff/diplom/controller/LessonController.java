package ru.tinkoff.diplom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.diplom.model.Lesson;
import ru.tinkoff.diplom.service.LessonService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/classes")
public class LessonController {
    @Autowired
    private LessonService lessonService;

    @GetMapping("/{id}")
    public Lesson getById(@PathVariable("id") int id) {
        return lessonService.getClassesById(id);
    }

    @GetMapping
    public List<Lesson> findAll() {
        return lessonService.findAllClasses();
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") int id) {
        lessonService.deleteClasses(id);
    }

    @PostMapping
    public void update(@RequestBody @Valid Lesson aClass) {
        lessonService.updateClasses(aClass);
    }

    @PutMapping
    public void save(@RequestBody @Valid Lesson aClass) {
        lessonService.saveClasses(aClass);
    }

    @PostMapping("/{id}/{teacherId}")
    public void updateTeacherForLesson(@PathVariable("id") int id, @PathVariable("teacherId") UUID teacherId){
        lessonService.updateTeacherForClasses(id, teacherId);
    }

}
