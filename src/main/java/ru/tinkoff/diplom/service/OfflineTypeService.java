package ru.tinkoff.diplom.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.diplom.model.courses.ConstrainToCourse;
import ru.tinkoff.diplom.model.type.OfflineValue;
import ru.tinkoff.diplom.repository.OfflineRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Service
public class OfflineTypeService implements BaseTypeService<OfflineValue> {

    @Autowired
    OfflineRepository repository;

    @Override
    public List<OfflineValue> findAllCourseType() {
        return repository.findAllCourseType();
    }

    @Override
    public Optional<OfflineValue> findCourseTypeById(int id) {
        return repository.findCourseTypeById(id);
    }

    @Override
    public void saveTypeToCourse(ConstrainToCourse constrainToCourse) {
        repository.saveTypeToCourse(constrainToCourse);
    }

    @Override
    @Transactional
    public void saveCourseType(@Valid OfflineValue courseType) {
        if (findCourseTypeById(courseType.getId()).isEmpty()) {
            repository.saveCourseType(courseType);
        } else
            throw new IllegalArgumentException("Can't save offline type because type with id=" + courseType.getId() + " is already exist");

    }

    @Override
    @Transactional
    public void updateCourseType(@Valid OfflineValue courseType) {
        findCourseTypeById(courseType.getId()).ifPresentOrElse(
                (online1 -> repository.updateCourseType(courseType)),
                () -> {
                    throw new IllegalArgumentException("Can't update offline type because type with id=" + courseType.getId() + " not exist");
                }
        );
    }

    @Override
    @Transactional
    public void deleteCourseType(int id) {
        findCourseTypeById(id).ifPresentOrElse(
                (online -> repository.deleteCourseType(id)),
                () -> {
                    throw new IllegalArgumentException("Can't delete offline type because type with id=" + id + " not exist");
                }
        );
    }

    public int findEmptyId() {
        return repository.findEmptyId();
    }
}
