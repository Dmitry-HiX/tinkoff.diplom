package ru.tinkoff.diplom.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.diplom.model.Schedule;
import ru.tinkoff.diplom.repository.ScheduleRepository;

import java.util.List;
import java.util.UUID;

@Service
public class ScheduleService {

    @Autowired
    private ScheduleRepository repository;

    @Transactional
    public List<Schedule> findScheduleById(UUID id) {
        if(repository.findScheduleById(id).isEmpty())
            throw new IllegalArgumentException("Schedule with id=" + id + " not exist");
        return repository.findScheduleById(id);
    }
}
