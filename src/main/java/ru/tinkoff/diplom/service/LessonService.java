package ru.tinkoff.diplom.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.diplom.model.Lesson;
import ru.tinkoff.diplom.repository.ClassRepository;
import ru.tinkoff.diplom.repository.TeacherRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class LessonService {
    @Autowired
    ClassRepository repository;
    @Autowired
    TeacherRepository teacherRepository;

    public Optional<Lesson> findClassesById(int id) {
        return repository.findClassesById(id);
    }

    public int findEmptyId() {
        return repository.findEmptyId();
    }

    public Lesson getClassesById(int id) {
        return findClassesById(id)
                .orElseThrow(() -> new IllegalArgumentException("Lesson with id=" + id + " not exist"));
    }

    @Transactional
    public void saveClasses(Lesson aClass) {
        if (findClassesById(aClass.getId()).isEmpty()) {
            repository.saveClasses(aClass);
        } else
            throw new IllegalArgumentException("Can't save lesson because lesson with id=" + aClass.getId() + " is already exist");
    }

    @Transactional
    public void updateClasses(Lesson aClass) {
        findClassesById(aClass.getId()).ifPresentOrElse(
                (classes1 -> repository.updateClasses(aClass)),
                () -> {
                    throw new IllegalArgumentException("Can't update lesson because lesson with id=" + aClass.getId() + " not exist");
                }
        );
    }

    @Transactional
    public void updateTeacherForClasses(int id, UUID teacherId) {
        findClassesById(id).ifPresentOrElse(
                (classes1 -> {
                    if (teacherRepository.findTeacherById(teacherId).isPresent()) {
                        classes1.setTeacherId(teacherId);
                        repository.updateClasses(classes1);
                    } else
                        throw new IllegalArgumentException("Can't update lesson because teacher with id=" + teacherId + " not exist");
                }),
                () -> {
                    throw new IllegalArgumentException("Can't update lesson because lesson with id=" + id + " not exist");
                }
        );
    }

    @Transactional
    public void deleteClasses(int id) {
        findClassesById(id).ifPresentOrElse(
                (classes -> {
                    if (repository.findSomeClassesToCourseById(id).isPresent())
                        repository.deleteClassesFromCourse(id);
                    repository.deleteClasses(id);
                }),
                () -> {
                    throw new IllegalArgumentException("Can't delete lesson because lesson with id=" + id + " not exist");
                }
        );
    }

    public List<Lesson> findAllClasses() {
        return repository.findAllClasses();
    }
}
