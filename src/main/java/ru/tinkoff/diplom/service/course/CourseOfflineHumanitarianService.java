package ru.tinkoff.diplom.service.course;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.diplom.model.Lesson;
import ru.tinkoff.diplom.model.User;
import ru.tinkoff.diplom.model.categories.Category;
import ru.tinkoff.diplom.model.categories.HumanitarianValue;
import ru.tinkoff.diplom.model.courses.ConstrainToCourse;
import ru.tinkoff.diplom.model.courses.Course;
import ru.tinkoff.diplom.model.courses.DefaultCourse;
import ru.tinkoff.diplom.model.type.OfflineValue;
import ru.tinkoff.diplom.model.type.Type;
import ru.tinkoff.diplom.repository.DefaultCourseRepository;
import ru.tinkoff.diplom.service.*;

import java.security.Principal;
import java.util.*;

@Service
public class CourseOfflineHumanitarianService<T, C, L> {
    private final UserService userService;
    private final LessonService lessonService;
    private final DefaultCourseRepository repository;
    private final OfflineTypeService offlineService;
    private final HumanitarianCategoryService humanitarianService;

    @Autowired
    public CourseOfflineHumanitarianService(DefaultCourseRepository repository,
                                            LessonService lessonService,
                                            UserService userService,
                                            OfflineTypeService offlineService,
                                            HumanitarianCategoryService humanitarianService) {
        this.lessonService = lessonService;
        this.userService = userService;
        this.repository = repository;
        this.offlineService = offlineService;
        this.humanitarianService = humanitarianService;
    }

    public HumanitarianValue findCategory(int id) {
        try {
            return humanitarianService.findCategoryById(id).orElseThrow();
        } catch (NoSuchElementException e) {
            throw new IllegalArgumentException("Wrong category data");
        }
    }

    public OfflineValue findType(int id) {
        try {
            return offlineService.findCourseTypeById(id).orElseThrow();
        } catch (NoSuchElementException e) {
            throw new IllegalArgumentException("Wrong type data");
        }
    }

    public Optional<DefaultCourse> findCourse(int idCourse) {
        return repository.findCourseById(idCourse);
    }

    public DefaultCourse getCourse(int id) {
        return findCourse(id).orElseThrow(
                () -> new IllegalArgumentException("Course with id=" + id + " not exist"));
    }

    @Transactional
    public Course<OfflineValue, HumanitarianValue, Lesson> getCourseWithTypeAndCategoryAndLessons(int idCourse) {
        DefaultCourse tempCourse = getCourse(idCourse);
        List<Lesson> lessons = repository.findClassesToCourseById(idCourse);
        OfflineValue typeOfCourse = findType(tempCourse.getTypeId());
        HumanitarianValue categoryOfCourse = findCategory(tempCourse.getCategoryId());
        return new Course<>(tempCourse.getId(), tempCourse.getName(), tempCourse.getDescription(),
                typeOfCourse, tempCourse.getCuratorId(), categoryOfCourse, lessons);
    }

    @Transactional
    public void saveCourse(Course<OfflineValue, HumanitarianValue, Lesson> course, Principal principal) {
        DefaultCourse newCourse = new DefaultCourse(course.getId(), course.getName(), course.getDescription(),
                course.getType().getId(), course.getCuratorId(), course.getCategory().getId(), course.getId());
        if (newCourse.getTypeId() == newCourse.getCategoryId() && newCourse.getTypeId() == newCourse.getId())
            try {
                repository.saveCourse(newCourse);
            } catch (Exception e) {
                throw new IllegalArgumentException("Course already exist");
            }
        else throw new IllegalArgumentException("Error category or type id");
        offlineService.saveTypeToCourse(new ConstrainToCourse(course.getId(), course.getType().getId()));
        humanitarianService.saveCategoryToCourse(new ConstrainToCourse(course.getId(), course.getCategory().getId()));
        offlineService.saveCourseType(course.getType());
        humanitarianService.saveCategory(course.getCategory());
        List<Lesson> lessons = course.getLessons();
        for (Lesson aClass : lessons) {
            lessonService.saveClasses(aClass);
            ConstrainToCourse lessonToCourse = new ConstrainToCourse(course.getId(), aClass.getId());
            saveClassesToCourse(lessonToCourse, principal);
        }
    }

    @Transactional
    public void updateCourse(Course<OfflineValue, HumanitarianValue, Lesson> course, Principal principal) {
        User user = userService.findUserByUsername(principal.getName());
        if (user.getAuthority().equals("ROLE_TEACHER")) {
            if (!(course.getCuratorId().equals(user.getId())))
                throw new IllegalArgumentException("It's not your course");
        }
        DefaultCourse newCourse = new DefaultCourse(course.getId(), course.getName(), course.getDescription(),
                course.getType().getId(), course.getCuratorId(), course.getCategory().getId(), course.getId());
        if (newCourse.getTypeId() == newCourse.getCategoryId() && newCourse.getTypeId() == newCourse.getId()) {
            repository.updateCourse(newCourse);
            offlineService.updateCourseType(course.getType());
            humanitarianService.updateCategory(course.getCategory());
            List<Lesson> lessons = course.getLessons();
            for (Lesson aClass : lessons) {
                lessonService.updateClasses(aClass);
            }
        } else throw new IllegalArgumentException("Wrong type or category data");
    }

    @Transactional
    public void deleteCourse(int id, Principal principal) {
        Optional<DefaultCourse> tempCourse = findCourse(id);
        User user = userService.findUserByUsername(principal.getName());
        if (tempCourse.isPresent())
            if (user.getAuthority().equals("ROLE_TEACHER"))
                if (!(tempCourse.get().getCuratorId().equals(user.getId())))
                    throw new IllegalArgumentException("It's not your course");
        if (tempCourse.isPresent()) {
            List<Lesson> lessons = repository.findClassesToCourseById(id);
            if (repository.findSomeClassesToCourseById(tempCourse.get().getId()).isPresent())
                repository.deleteClassesFromCourse(tempCourse.get().getId());
            for (Lesson aClass : lessons) {
                lessonService.deleteClasses(aClass.getId());
            }
            repository.deleteCourse(id);
        } else throw new IllegalArgumentException("Course not found with id=" + id);
    }

    @Transactional
    public void saveClassesToCourse(ConstrainToCourse constrainToCourse, Principal principal) {
        Optional<DefaultCourse> tempCourse = findCourse(constrainToCourse.getCourseId());
        lessonService.getClassesById(constrainToCourse.getConstrainId());
        User user = userService.findUserByUsername(principal.getName());
        if (tempCourse.isPresent())
            if (user.getAuthority().equals("ROLE_TEACHER"))
                if (!(tempCourse.get().getCuratorId().equals(user.getId())))
                    throw new IllegalArgumentException("It's not your course");
        repository.saveClassesToCourse(constrainToCourse);
    }

    @Transactional
    public void copyCourse(Course<OfflineValue, HumanitarianValue, Lesson> course, Principal principal) {
        User user = userService.findUserByUsername(principal.getName());
        DefaultCourse tempCourse = getCourse(course.getId());
        OfflineValue typeOfCourse;
        HumanitarianValue categoryOfCourse;
        try {
            typeOfCourse = (OfflineValue) findType(course.getId());
            categoryOfCourse = (HumanitarianValue) findCategory(course.getId());
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("Wrong type or category data");
        }
        int idNewCourse = repository.findEmptyId();
        typeOfCourse.setId(idNewCourse);
        categoryOfCourse.setId(idNewCourse);
        repository.saveCourse(new DefaultCourse(idNewCourse, tempCourse.getName(), tempCourse.getDescription(),
                typeOfCourse.getId(), user.getId(), categoryOfCourse.getId(), idNewCourse));
        offlineService.saveTypeToCourse(new ConstrainToCourse(idNewCourse, idNewCourse));
        humanitarianService.saveCategoryToCourse(new ConstrainToCourse(idNewCourse, idNewCourse));
        offlineService.saveCourseType(typeOfCourse);
        humanitarianService.saveCategory(categoryOfCourse);
        List<Lesson> lessons = course.getLessons();
        for (Lesson aClass : lessons) {
            int a = lessonService.findEmptyId();
            aClass.setId(a);
            aClass.setDate(null);
            lessonService.saveClasses(aClass);
            ConstrainToCourse lessonToCourse = new ConstrainToCourse(idNewCourse, a);
            saveClassesToCourse(lessonToCourse, principal);
        }
    }

    public List<DefaultCourse> findAll() {
        return repository.findAllCourse();
    }

    public List<DefaultCourse> getAllCourseOfflineHumanitarian() {
        return repository.getAllCourseOfflineHumanitarian();
    }
}
