package ru.tinkoff.diplom.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.diplom.model.Teacher;
import ru.tinkoff.diplom.model.User;
import ru.tinkoff.diplom.repository.TeacherRepository;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TeacherService {
    private final TeacherRepository repository;
    private final UserService userService;
    @Autowired
    public TeacherService(TeacherRepository repository, UserService userService) {
        this.repository = repository;
        this.userService = userService;
    }

    public Optional<Teacher> findTeacherById(UUID id) {
        return repository.findTeacherById(id);
    }

    @Transactional
    public Teacher getTeacherById(UUID id) {
        return findTeacherById(id)
                .orElseThrow(() -> new IllegalArgumentException("Teacher with id=" + id + " not exist"));
    }

    @Transactional
    public void saveTeacher(Teacher teacher) {
        if (findTeacherById(teacher.getId()).isEmpty()) {
            repository.saveTeacher(teacher);
        } else
            throw new IllegalArgumentException("Can't save teacher because teacher with id=" + teacher.getId() + " is already exist");
    }

    @Transactional
    public void updateTeacher(Teacher teacher, Principal principal) {
        findTeacherById(teacher.getId()).ifPresentOrElse(
                (teacher1 ->{
                    User user = userService.findUserByUsername(principal.getName());
                    if(user.getAuthority().equals("ROLE_TEACHER")){
                        if(teacher.getId().equals(user.getId()))
                            repository.updateTeacher(teacher);
                        else throw new IllegalArgumentException("It's not your profile");
                    } else repository.updateTeacher(teacher);
                }),
                () -> {
                    throw new IllegalArgumentException("Can't update teacher because teacher with id=" + teacher.getId() + " not exist");
                }
        );
    }

    @Transactional
    public void deleteTeacher(UUID id) {
        findTeacherById(id).ifPresentOrElse(
                (teacher -> repository.deleteTeacher(id)),
                () -> {
                    throw new IllegalArgumentException("Can't delete teacher because teacher with id=" + id + " not exist");
                }
        );
    }

    public List<Teacher> findAllTeacher() {
        return repository.findAllTeacher();
    }
}
