package ru.tinkoff.diplom.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.diplom.model.User;
import ru.tinkoff.diplom.repository.UserRepository;

import java.util.*;

@Service
public class UserService implements UserDetailsService{

    @Autowired
    private UserRepository repository;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public Optional<User> findUserById(UUID id) {
        return repository.findUserById(id);
    }

    public User getUserById(UUID id) {
        return findUserById(id)
                .orElseThrow(() -> new IllegalArgumentException("User with id=" + id + " not exist"));
    }

    public User getUserByUsername(String name) {
        return findUserByUsername(name);
    }

    public User findUserByUsername(String name) {
        return repository.findUserByUsername(name);
    }

    @Transactional
    public void saveUser(User user) {
        if (findUserById(user.getId()).isEmpty()) {
            String hashedPassword = passwordEncoder().encode(user.getPassword());
            user.setPassword(hashedPassword);
            repository.saveUser(user);
        }
        else
            throw new IllegalArgumentException("Can't save user because user with id=" + user.getId() + " is already exist");
    }

    @Transactional
    public void updateUser(User user) {
        findUserById(user.getId()).ifPresentOrElse(
                (user1 -> {
                    String hashedPassword = passwordEncoder().encode(user.getPassword());
                    user.setPassword(hashedPassword);
                    repository.updateUser(user);
                }),
                () -> {
                    throw new IllegalArgumentException("Can't update user because user with id=" + user.getId() + " not exist");
                }
        );
    }

    @Transactional
    public void deleteUser(UUID id) {
        findUserById(id).ifPresentOrElse(
                (user1 -> repository.deleteUser(id)),
                () -> {
                    throw new IllegalArgumentException("Can't delete user because user with id=" + id + " not exist");
                }
        );
    }


    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findUserByUsername(username);
        if(user==null)
            throw new UsernameNotFoundException(String.format("User '%s' not found", username));

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), mapRoles(user));
    }

    private Collection<? extends GrantedAuthority> mapRoles(User user){
        return Collections.singleton(new SimpleGrantedAuthority(user.getAuthority()));
    }
}
