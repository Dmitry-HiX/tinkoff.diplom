package ru.tinkoff.diplom.service;

import ru.tinkoff.diplom.model.courses.ConstrainToCourse;
import ru.tinkoff.diplom.model.type.Type;

import java.util.List;
import java.util.Optional;

public interface BaseTypeService <T extends Type>{

    List<T> findAllCourseType();

    void saveCourseType(T courseType);

    void updateCourseType(T courseType);

    void deleteCourseType(int id);

    Optional <T> findCourseTypeById(int id);

    void saveTypeToCourse(ConstrainToCourse constrainToCourse);
}
