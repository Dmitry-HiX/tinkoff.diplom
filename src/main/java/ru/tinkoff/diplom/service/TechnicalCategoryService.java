package ru.tinkoff.diplom.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.diplom.model.categories.TechnicalValue;
import ru.tinkoff.diplom.model.courses.ConstrainToCourse;
import ru.tinkoff.diplom.repository.TechnicalRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TechnicalCategoryService implements BaseCategoryService<TechnicalValue> {
    @Autowired
    TechnicalRepository repository;

    public TechnicalValue getTechnicalById(int id) {
        return findCategoryById(id)
                .orElseThrow(() -> new IllegalArgumentException("Category with id=" + id + " not exist"));
    }

    public List<TechnicalValue> findAllCategory() {
        return repository.findAllCategory();
    }

    @Override
    @Transactional
    public void saveCategory(TechnicalValue category) {
        if (findCategoryById(category.getId()).isEmpty()) {
            repository.saveCategory(category);
        } else
            throw new IllegalArgumentException("Can't save category because category with id=" + category.getId() + " is already exist");
    }

    @Override
    @Transactional
    public void updateCategory(TechnicalValue category) {
        findCategoryById(category.getId()).ifPresentOrElse(
                (category1 -> repository.updateCategory(category)),
                () -> {
                    throw new IllegalArgumentException("Can't update category because category with id=" + category.getId() + " not exist");
                }
        );
    }

    @Override
    @Transactional
    public void deleteCategory(int id) {
        findCategoryById(id).ifPresentOrElse(
                (category -> repository.deleteCategory(id)),
                () -> {
                    throw new IllegalArgumentException("Can't delete category because category with id=" + id + " not exist");
                }
        );
    }

    @Override
    public Optional<TechnicalValue> findCategoryById(int id) {
        return repository.findCategoryById(id);
    }

    public int findEmptyId() {
        return repository.findEmptyId();
    }

    @Override
    public void saveCategoryToCourse(ConstrainToCourse constrainToCourse){repository.saveCategoryToCourse(constrainToCourse);}
}