package ru.tinkoff.diplom.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tinkoff.diplom.model.courses.ConstrainToCourse;
import ru.tinkoff.diplom.model.type.OnlineValue;
import ru.tinkoff.diplom.repository.OnlineRepository;

import java.util.List;
import java.util.Optional;

@Service
public class OnlineTypeService implements BaseTypeService<OnlineValue> {

    @Autowired
    OnlineRepository repository;

    @Override
    public Optional<OnlineValue> findCourseTypeById(int id) {
        return repository.findCourseTypeById(id);
    }

    @Override
    public List<OnlineValue> findAllCourseType() {
        return repository.findAllCourseType();
    }

    @Override
    public void saveCourseType(OnlineValue courseType) {
        if (findCourseTypeById(courseType.getId()).isEmpty()) {
            repository.saveCourseType(courseType);
        } else
            throw new IllegalArgumentException("Can't save online type because type with id=" + courseType.getId() + " is already exist");

    }

    @Override
    public void updateCourseType(OnlineValue courseType) {
        findCourseTypeById(courseType.getId()).ifPresentOrElse(
                (online1 -> repository.updateCourseType(courseType)),
                () -> {
                    throw new IllegalArgumentException("Can't update online type because type with id=" + courseType.getId() + " not exist");
                }
        );
    }

    @Override
    public void deleteCourseType(int id) {
        findCourseTypeById(id).ifPresentOrElse(
                (online -> repository.deleteCourseType(id)),
                () -> {
                    throw new IllegalArgumentException("Can't delete online type because type with id=" + id + " not exist");
                }
        );
    }

    public int findEmptyId() {
        return repository.findEmptyId();
    }

    @Override
    public void saveTypeToCourse(ConstrainToCourse constrainToCourse){repository.saveTypeToCourse(constrainToCourse);}
}
