package ru.tinkoff.diplom.service;

import ru.tinkoff.diplom.model.categories.Category;
import ru.tinkoff.diplom.model.courses.ConstrainToCourse;

import java.util.List;
import java.util.Optional;

public interface BaseCategoryService<T extends Category>{

    List<T> findAllCategory();

    void saveCategory(T category);

    void updateCategory(T categoryValue);

    void deleteCategory(int id);

    Optional<T> findCategoryById(int id);

    void saveCategoryToCourse(ConstrainToCourse constrainToCourse);
}
