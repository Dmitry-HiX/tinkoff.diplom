package ru.tinkoff.diplom.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.diplom.model.courses.ConstrainToCourse;
import ru.tinkoff.diplom.model.type.OnlineValue;

import java.util.List;
import java.util.Optional;

@Mapper
public interface OnlineRepository {
    List<OnlineValue> findAllCourseType();

    void saveCourseType(OnlineValue courseType);

    void updateCourseType(OnlineValue courseType);

    void deleteCourseType(int id);

    Optional<OnlineValue> findCourseTypeById(int id);

    int findEmptyId();

    void saveTypeToCourse(ConstrainToCourse constrainToCourse);
}
