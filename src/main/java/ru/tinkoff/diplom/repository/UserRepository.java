package ru.tinkoff.diplom.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.diplom.model.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface UserRepository {

    void saveUser(User user);

    void updateUser(User user);

    void deleteUser(UUID id);

    Optional<User> findUserById(UUID id);

    User findUserByUsername(String name);

    List<User> findAllUser();
}
