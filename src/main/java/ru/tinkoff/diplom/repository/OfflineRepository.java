package ru.tinkoff.diplom.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.diplom.model.courses.ConstrainToCourse;
import ru.tinkoff.diplom.model.type.OfflineValue;

import java.util.List;
import java.util.Optional;

@Mapper
public interface OfflineRepository {
    List<OfflineValue> findAllCourseType();

    void saveCourseType(OfflineValue courseType);

    void updateCourseType(OfflineValue courseType);

    void deleteCourseType(int id);

    Optional<OfflineValue> findCourseTypeById(int id);

    void saveTypeToCourse(ConstrainToCourse constrainToCourse);

    int findEmptyId();
}
