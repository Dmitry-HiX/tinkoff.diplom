package ru.tinkoff.diplom.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.diplom.model.Lesson;

import java.util.List;
import java.util.Optional;

@Mapper
public interface ClassRepository {
    List<Lesson> findAllClasses();

    void saveClasses(Lesson aClass);

    void updateClasses(Lesson aClass);

    void deleteClasses(int id);

    Optional<Lesson> findClassesById(int id);

    int findEmptyId();

    Optional<Integer> findSomeClassesToCourseById(int id);

    void deleteClassesFromCourse(int id);
}
