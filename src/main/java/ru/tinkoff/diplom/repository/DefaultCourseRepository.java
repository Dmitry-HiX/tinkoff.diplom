package ru.tinkoff.diplom.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.diplom.model.Lesson;
import ru.tinkoff.diplom.model.courses.ConstrainToCourse;
import ru.tinkoff.diplom.model.courses.DefaultCourse;

import java.util.List;
import java.util.Optional;

@Mapper
public interface DefaultCourseRepository {
    List<DefaultCourse> findAllCourse();

    void saveCourse(DefaultCourse course);

    void updateCourse(DefaultCourse course);

    void deleteCourse(int id);

    Optional<DefaultCourse> findCourseById(int id);

    void saveClassesToCourse(ConstrainToCourse lessonToCourse);

    void deleteClassesFromCourse(int id);

    Optional<Integer> findSomeClassesToCourseById(int id);

    List<Lesson> findClassesToCourseById(int id);

    Optional<DefaultCourse> findCourseByTypeId(int id);

    Optional<DefaultCourse> findCourseByCategoryId(int id);

    int findEmptyId();

    List<DefaultCourse> getAllCourseOnlineTechnical();

    List<DefaultCourse> getAllCourseOnlineHumanitarian();

    List<DefaultCourse> getAllCourseOfflineHumanitarian();

    List<DefaultCourse> getAllCourseOfflineTechnical();
}
