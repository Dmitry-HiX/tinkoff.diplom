package ru.tinkoff.diplom.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.diplom.model.categories.HumanitarianValue;
import ru.tinkoff.diplom.model.courses.ConstrainToCourse;

import java.util.List;
import java.util.Optional;

@Mapper
public interface HumanitarianRepository {

    List<HumanitarianValue> findAllCategory();

    void saveCategory(HumanitarianValue categoryValue);

    void updateCategory(HumanitarianValue categoryValue);

    void deleteCategory(int id);

    Optional<HumanitarianValue> findCategoryById(int id);

    void saveCategoryToCourse(ConstrainToCourse constrainToCourse);

    int findEmptyId();
}
