package ru.tinkoff.diplom.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.diplom.model.categories.TechnicalValue;
import ru.tinkoff.diplom.model.courses.ConstrainToCourse;

import java.util.List;
import java.util.Optional;
@Mapper
public interface TechnicalRepository{

    List<TechnicalValue> findAllCategory();

    void saveCategory(TechnicalValue categoryValue);

    void updateCategory(TechnicalValue categoryValue);

    void deleteCategory(int id);

    Optional<TechnicalValue> findCategoryById(int id);

    int findEmptyId();

    void saveCategoryToCourse(ConstrainToCourse constrainToCourse);
}
