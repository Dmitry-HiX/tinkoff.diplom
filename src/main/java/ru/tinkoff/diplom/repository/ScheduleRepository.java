package ru.tinkoff.diplom.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.diplom.model.Schedule;

import java.util.List;
import java.util.UUID;
@Mapper
public interface ScheduleRepository {
    List<Schedule>findScheduleById(UUID id);
}
