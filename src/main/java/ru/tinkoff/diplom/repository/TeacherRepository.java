package ru.tinkoff.diplom.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.diplom.model.Teacher;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Mapper
public interface TeacherRepository {
    List<Teacher> findAllTeacher();

    void saveTeacher(Teacher teacher);

    void updateTeacher(Teacher teacher);

    void deleteTeacher(UUID id);

    Optional<Teacher> findTeacherById(UUID id);
}
