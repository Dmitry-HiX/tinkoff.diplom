CREATE TABLE LIST_OF_CLASSES
(
    id int,
    list_id_classes INT UNIQUE NOT NULL,
    PRIMARY KEY(id, list_id_classes)
);

CREATE TABLE TEACHER
(
    id UUID PRIMARY KEY,
    lastname VARCHAR(64) NOT NULL,
    firstname VARCHAR(64) NOT NULL,
    second_name VARCHAR(64) NOT NULL,
    age INT NOT NULL
);

CREATE TABLE USERS(
    id UUID primary key,
    username VARCHAR(100) not null UNIQUE,
    password VARCHAR(100) not null,
    authority VARCHAR(100) not null DEFAULT 'ROLE_TEACHER',
    enabled boolean not null
);

CREATE TABLE CLASSES
(
    id int PRIMARY KEY,
    name VARCHAR(64) NOT NULL,
    description VARCHAR(64) NOT NULL,
    teacher_id UUID NOT NULL,
    date timestamp
);

ALTER TABLE TEACHER
    ADD FOREIGN KEY (id)
        REFERENCES USERS(id);

CREATE INDEX list_of_classes_idx
ON LIST_OF_CLASSES (id, list_id_classes);

ALTER TABLE LIST_OF_CLASSES
    ADD FOREIGN KEY (list_id_classes)
        REFERENCES CLASSES(id);

ALTER TABLE LIST_OF_CLASSES
    ADD FOREIGN KEY (id)
        REFERENCES COURSE(list_of_classes);

ALTER TABLE CLASSES
    ADD FOREIGN KEY (teacher_id)
        REFERENCES TEACHER(id);

ALTER TABLE COURSE
    ADD FOREIGN KEY (curator_id)
        REFERENCES TEACHER(id);