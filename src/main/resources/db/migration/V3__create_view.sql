Create View Schedule AS
Select
teacher.id as id,
teacher.lastname ||' '|| LEFT(teacher.firstname,1)|| '.' || LEFT(teacher.second_name,1)||'.' AS fio_teacher,
course.name as course_name,
classes.name as classes_name,
classes.date as date
FROM teacher
LEFT JOIN classes ON classes.teacher_id = teacher.id
LEFT JOIN list_of_classes ON list_of_classes.list_id_classes = classes.id
LEFT JOIN course ON course.id = list_of_classes.id
GROUP BY teacher.id, fio_teacher, course_name, classes_name, date
ORDER BY (date);