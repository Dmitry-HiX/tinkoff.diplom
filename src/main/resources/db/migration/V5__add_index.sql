CREATE INDEX TYPE_OF_COURSE_idx
ON TYPE_OF_COURSE (id, type_id);

CREATE INDEX CATEGORY_idx
ON CATEGORY (id, category_id);

CREATE INDEX Course_id_idx
ON COURSE (id);
CREATE INDEX Technical_id_idx
ON Technical (id);
CREATE INDEX Humanitarian_id_idx
ON Humanitarian (id);
CREATE INDEX Online_id_idx
ON Online (id);
CREATE INDEX Offline_id_idx
ON Offline (id);
CREATE INDEX TEACHER_id_idx
ON TEACHER (id);
CREATE INDEX USERS_id_idx
ON USERS (id);
CREATE INDEX USERS_username_idx
ON USERS (username);
CREATE INDEX CLASSES_id_idx
ON CLASSES (id);